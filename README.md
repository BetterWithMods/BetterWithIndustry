# Better With Industry

BWI is an technological expansion mod intended to extend the progress of Better With Mods world to an Industrial Revolution. Keeping in mind the core values and design, this mod will add many unique highly automate-able machines that require unique mechanisms that interact dyanmically in unusual ways.

The mod is not entirely thought out as of yet, but there are a few central ideas that are already in my mind about how I want to go about it.
Some of the ideas are in this project's [Development Board](https://gitlab.com/BetterWithMods/BetterWithIndustry/boards/651739?=), if you also have cool ideas in a theme and manner that is congruent with that of BWM feel free to suggest them (Disclaimer: there is a high chance it will be declined or ammended to be more fitting). 