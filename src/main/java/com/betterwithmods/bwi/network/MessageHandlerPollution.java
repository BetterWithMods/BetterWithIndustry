package com.betterwithmods.bwi.network;

import betterwithmods.network.handler.BWMessageHandler;
import com.betterwithmods.bwi.BetterWithIndustry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageHandlerPollution extends BWMessageHandler<MessagePollution> {
    @Override
    public void handleMessage(MessagePollution message, MessageContext context) {
        BetterWithIndustry.proxy.syncPollution(message.getPos(), message.getPollution());
    }
}
