package com.betterwithmods.bwi.modules.world.pollution;

import betterwithmods.module.Feature;
import com.betterwithmods.bwi.BetterWithIndustry;
import com.betterwithmods.bwi.api.pollution.IPollutant;
import com.betterwithmods.bwi.api.pollution.Pollutant;
import com.betterwithmods.bwi.api.pollution.Pollution;
import com.betterwithmods.bwi.network.MessagePollution;
import com.betterwithmods.bwi.network.Network;
import com.betterwithmods.bwi.util.WorldUtils;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class WorldPollution extends Feature {
    private static ResourceLocation CHUNK_POLLUTION = new ResourceLocation(BetterWithIndustry.MODID, "chunk_pollution");

    public static Pollution getPollution(World world, BlockPos pos) {
        Chunk chunk = world.getChunkFromBlockCoords(pos);
        return chunk.getCapability(Pollution.CAPABILITY_POLLUTION, null);
    }

    @Override
    public void preInit(FMLPreInitializationEvent event) {
        CapabilityManager.INSTANCE.register(Pollution.class, new Pollution.Storage(), Pollution::new);
        CapabilityManager.INSTANCE.register(IPollutant.class, new Pollutant.Storage(), Pollutant::new);
    }

    @SubscribeEvent
    public void attachCapability(AttachCapabilitiesEvent<Chunk> event) {
        if (!event.getCapabilities().containsKey(CHUNK_POLLUTION)) {
            event.addCapability(CHUNK_POLLUTION, new Pollution());
        }
    }

    @SubscribeEvent
    public void onChunkLoad(ChunkEvent.Load event) {
        Pollution p = event.getChunk().getCapability(Pollution.CAPABILITY_POLLUTION, null);
        if (p != null) {
            ChunkPos pos = event.getChunk().getPos();
            Network.sendToAllAround(new MessagePollution(pos, p.getPollution()), event.getWorld(), new BlockPos(pos.getXStart(), 128, pos.getZStart()));
        }
    }

    @SubscribeEvent
    public void onTick(TickEvent.WorldTickEvent event) {
        //Get pollution from pollutants
        WorldUtils.forEachTile(event.world, tile -> tile.hasCapability(Pollution.CAPABILITY_POLLUTION, null), tile -> {
            BlockPos pos = tile.getPos();
            World world = tile.getWorld();

            Pollution chunkPollution = getPollution(world, pos);
            if (chunkPollution != null) {
                IPollutant p = tile.getCapability(Pollutant.CAPABILITY_POLLUTION_SOURCE, EnumFacing.UP);
                if (p != null && p.isPolluting()) {
                    chunkPollution.addPollution(p.getPollutionOutput());
                    Network.sendToAllAround(new MessagePollution(new ChunkPos(pos), chunkPollution.getPollution()), world, pos);
                }
            }
        });
    }


    @Override
    public void serverStarting(FMLServerStartingEvent event) {
        event.registerServerCommand(new PollutionCommand());
    }

    @Override
    public String getFeatureDescription() {
        return "Add Pollutions to the world";
    }

    @Override
    public boolean hasSubscriptions() {
        return true;
    }

}
